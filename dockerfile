FROM node:13.5-alpine


RUN mkdir /app
WORKDIR /app

ADD circom ./circom
ADD circom-output ./circom-output
ADD src ./src
ADD *.json ./
ADD *.js ./
ADD .babelrc ./.babelrc

RUN npm install
# RUN ./node_modules/.bin/webpack --mode production --config webpack.server.config.js
# RUN ./node_modules/.bin/webpack --mode production --config webpack.prod.config.js 

RUN npm run buildProd

EXPOSE 8080

CMD ["npm", "run", "start"]
