include "comparators.circom";
include "poseidon.circom";

template SMTHash1() {
    signal input serectKey;
    signal input value;
    signal output out;

    component h = Poseidon(2, 6, 8, 57);   // Constant
    h.inputs[0] <== serectKey;
    h.inputs[1] <== value;

    out <== h.out;
}

template FieldValueHash(){
    signal private input value;
    signal private input serectKey;
    signal input field;
    
    signal output out;

    component fieldHash = SMTHash1();
    fieldHash.value <== field;
    fieldHash.serectKey <== serectKey;

    component valueHash = SMTHash1();
    valueHash.value <== value;
    valueHash.serectKey <== serectKey;

    component hash = SMTHash1();
    hash.value <== fieldHash.out;
    hash.serectKey <== valueHash.out;

    out <== hash.out;
}

template LessThanVerify() {
    signal input expected;
    signal private input serectKey;
    signal private input value;
    signal input field;
    signal input hash;

    component fvh = FieldValueHash();
    fvh.field <== field;
    fvh.value <== value;
    fvh.serectKey <== serectKey;

    hash === fvh.out;

    component lt = LessThan();
    lt.in[0] <== value;
    lt.in[1] <== expected;

    lt.out === 1;
}

template GreaterThanVerify() {
    signal input expected;
    signal private input serectKey;
    signal private input value;
    signal input field;
    signal input hash;

    component fvh = FieldValueHash();
    fvh.field <== field;
    fvh.value <== value;
    fvh.serectKey <== serectKey;
    
    hash === fvh.out;

    component lt = GreaterThan();
    lt.in[0] <== value;
    lt.in[1] <== expected;

    lt.out === 1;
}

template LessThanEqualVerify() {
    signal input expected;
    signal private input serectKey;
    signal private input value;
    signal input field;
    signal input hash;

    component fvh = FieldValueHash();
    fvh.field <== field;
    fvh.value <== value;
    fvh.serectKey <== serectKey;

    hash === fvh.out;

    component lt = LessEqThan();
    lt.in[0] <== value;
    lt.in[1] <== expected;

    lt.out === 1;
}

template GreaterThanEqualVerify() {
    signal input expected;
    signal private input serectKey;
    signal private input value;
    signal input field;
    signal input hash;

    component fvh = FieldValueHash();
    fvh.field <== field;
    fvh.value <== value;
    fvh.serectKey <== serectKey;

    hash === fvh.out;

    component lt = GreaterEqThan();
    lt.in[0] <== value;
    lt.in[1] <== expected;

    lt.out === 1;
}

template EqualVerify() {
    signal input expected;
    signal private input serectKey;
    signal private input value;
    signal input field;
    signal input hash;

    component fvh = FieldValueHash();
    fvh.field <== field;
    fvh.value <== value;
    fvh.serectKey <== serectKey;

    hash === fvh.out;
    value === expected;
}

template test() {
    signal private input serectKey;
    %code%
}

component main = test(); 
