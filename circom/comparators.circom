template Num2Bits(n) {
    signal input in;
    signal output out[n];
    for (var i = 0; i<n; i++) {
        out[i] <-- (in >> i) & 1;
    }
}

template Bits2Num(n) {
    signal input in[n];
    signal output out;
    var lc1=0;

    for (var i = 0; i<n; i++) {
        lc1 += in[i] * 2**i;
    }

    lc1 ==> out;
}


template LessThan() {
    signal input in[2];
    signal output out;

    component n2b = Num2Bits(65);

    n2b.in <== in[0]+ (1<<32) - in[1];

    out <== 1-n2b.out[32];
}

template LessEqThan() {
    signal input in[2];
    signal output out;

    component lt = LessThan();

    lt.in[0] <== in[0];
    lt.in[1] <== in[1]+1;
    lt.out ==> out;
}

template GreaterThan() {
    signal input in[2];
    signal output out;

    component lt = LessThan();

    lt.in[0] <== in[1];
    lt.in[1] <== in[0];
    lt.out ==> out;
}

template GreaterEqThan() {
    signal input in[2];
    signal output out;

    component lt = LessThan();

    lt.in[0] <== in[1];
    lt.in[1] <== in[0]+1;
    lt.out ==> out;
}

