
const fs = require('fs')
const snarkjs = require("snarkjs");
const compiler = require("circom");

const generate = (query, signature) => {

    let i = 1
    let code = ''
    let verifyInput = {};

    Object.keys(query).forEach(field => {
        const criteria = query[field];
        Object.keys(criteria).forEach(operand => {
            const expectedValue = criteria[operand]
            const userInfo = signature.userInfo.filter(userInfo => {
                return userInfo.id == field
            }).shift()
            const label = userInfo.label
            const hash = userInfo.hash

            let eqLabel = `eq${i}`;

            verifyInput[`${eqLabel}Expected`] = [field, expectedValue]
            verifyInput[`${eqLabel}Value`] = [field, 'value']
            verifyInput[`${eqLabel}Field`] = [field, 'id']

            let snippet = `
            signal input ${eqLabel}Expected;
            signal private input ${eqLabel}Value;
            signal input ${eqLabel}Field;
            var ${eqLabel}Hash = ${hash};\n
            `

            switch (operand) {
                case '$eq':
                    snippet += `component ${eqLabel} = EqualVerify();`;
                    break;
                case '$lt':
                    snippet += `component ${eqLabel} = LessThanVerify();`;
                    break;
                case '$lte':
                    snippet += `component ${eqLabel} = LessThanEqualVerify();`;
                    break;
                case '$gt':
                    snippet += `component ${eqLabel} = GreaterThanVerify();`;
                    break;
                case '$gte':
                    snippet += `component ${eqLabel} = GreaterThanEqualVerify();`;
                    break;
                default:
                    throw Error('Invalid Operand.')
            }
            snippet += `
            ${eqLabel}.serectKey <== serectKey;
            ${eqLabel}.expected <== ${eqLabel}Expected;
            ${eqLabel}.value <== ${eqLabel}Value;
            ${eqLabel}.field <== ${eqLabel}Field;
            ${eqLabel}.hash <== ${eqLabel}Hash;
            `
            code += snippet
            i++
        })
    })
    return {
        userId: signature.userId,
        code: code,
        verifyInput: verifyInput
    }
}


export default (app) => {

    app.post('/api', (req, res) => {
        console.log(req.body)
        const { query, signature } = req.body
        const { code, verifyInput, userId } = generate(query, signature)

        const buff = fs.readFileSync('./circom/main.circom')
        let content = buff.toString('utf-8')
        content = content.replace('%code%', code)
        fs.writeFileSync(`./circom/${userId}.circom`, content);

        compiler(`./circom/${userId}.circom`).then(compiled => {
            fs.writeFileSync(`./circom-output/${userId}.json`, JSON.stringify(compiled));
            const circuit = new snarkjs.Circuit(compiled);
            const setup = snarkjs.groth.setup(circuit);
            const {vk_proof, vk_verifier} = snarkjs.stringifyBigInts(setup);
            setup.toxic
            res.json({
                data: {
                    compiled: compiled,
                    vk_proof: vk_proof,
                    vk_verifier: vk_verifier,
                    verifyInput: verifyInput
                }
            })
        }).catch(err => {
            res.status(409).json({
                error: err
            })
        })
    })

    app.get('*', (req, res) => {
        res.redirect('/')
    })

}