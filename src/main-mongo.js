const express = require('express')
const bodyParser = require('body-parser')
const hashSortCoerce = require('node-object-hash')({ sort: true, coerce: true });
const uuid = require('uuid').v4;
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const url = 'mongodb://admin:password@localhost:27017';
const dbName = 'myproject';

const app = express()
let db

MongoClient.connect(url)
   .then(function (mongoClient) {
      console.log("Connected successfully to server")
      db = mongoClient.db(dbName)
   }).catch(console.error)

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

const hash = (obj) => hashSortCoerce.hash(obj).toString()

const createToken = (userId, token) => db.collection("userToken").insertOne({ userId: userId, token: token })
const touchToken = (token) => db.collection("userToken").findOne({ token: token }).then(doc => {
   if(doc){
      return doc.userId
   }else{
      throw new Error()
   }
});
const revokeToken = (token) => db.collection("userToken").deleteOne({ token: token })

const currentTimestamp = () => {
   const dateTime = Date.now();
   const timestamp = Math.floor(dateTime / 1000);
   return timestamp;
};

const notFound = (req, res) => {
   res.status(404);
   res.send("not found")
}
const error = (req, res) => {
   res.status(409);
   res.send("error")
}

app.get('/', (req, res) => {
   res.send('Hello World');
})

app.get('/profile', (req, res) => {
   let userId = req.query.userId;
   if (!userId) return notFound(req, res)
   db.collection("user").findOne({ _id: userId }).then(data => {
      let vaild = hash({
         _id: data._id,
         data: data.data,
      }) == data.hash;

      if (vaild) {
         res.send({
            userId: data._id,
            data: data.data
         })
      } else {
         error(req, res);
      }

   })
})

app.post('/profile/register', (req, res) => {
   let data = req.body.data
   let userId = "";
   for (let i = 0; i < 10; i++) {
      if (i == 0) {
         userId += Math.random() * 9 | 0 + 1
      } else {
         userId += Math.random() * 9 | 0
      }
   }
   let doc = {
      _id: userId,
      data: data,
      createAt: currentTimestamp(),
   }
   doc.hash = hash(doc);
   db.collection("user").insertOne(doc).then(() => {
      let token = uuid();
      createToken(userId, token).then(() => res.send({
         "token": token
      }))
   })
})


app.post('/profile/active', (req, res) => {
   let data = req.body
   let token = data.token
   let userIdHash = data.userIdHash
   touchToken(token)
      .then(_id => {
         revokeToken(token)
         return db.collection("user").findOneAndUpdate({ _id }, {'$set':{userIdHash}}, {upsert:true}).then(result=>result.value)
      })
      .then(doc => {
         let vaild = hash({
            _id: doc._id,
            data: doc.data,
            timestamp: doc.timestamp
         }) == doc.hash;
         if (vaild) {
            res.send({
               userId: doc._id,
               data: doc.data
            })
         } else {
            error(req, res);
         }
      }).catch(()=>error(req, res))
})


app.post('/profile/update', (req, res) => {
   res.send('Hello World');
})


const server = app.listen(3000, () => {
   const host = server.address().address
   const port = server.address().port
   console.log(`http://${host}:${port}`)
})
