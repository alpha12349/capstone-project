import './css/bootstrap.min.css'
import './css/style.css'

import registration from './js/component-registration'
import trustedSetup from './js/component-trustedSetup'
import proofGeneration from './js/component-proofGeneration'
import verifier from './js/component-verifier'


// Needed for Hot Module Replacement
if(typeof(module.hot) !== 'undefined') {
  module.hot.accept() // eslint-disable-line no-undef  
}

registration()
trustedSetup()
proofGeneration()
verifier()
