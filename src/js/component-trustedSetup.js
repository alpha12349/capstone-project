import jQuery from 'jquery'
import store from './store'
import snarkjs from 'snarkjs'

let numOfProofGen = 1

const proofGen = (query) => {
  const userSignature = store.get('userSignature')
  console.log({
    query: query,
    signature: userSignature,
  })

  const userInput = {};
  const verifyInput = {
    eq1Expected: ["87124", "18"],
    eq1Value: ["87124", "value"],
    eq1Field: ["87124", "id"]
  }


  jQuery.ajax({
    type: 'POST',
    url: '/api',
    contentType: "application/json; charset=utf-8",
    data: JSON.stringify({
      query: query,
      signature: userSignature,
    }),
    success: (body) => {
      const data = body.data
      store.set("compiled", data.compiled)
      store.set("vk_proof", data.vk_proof)
      store.set("vk_verifier", data.vk_verifier)
      store.set("verifyInput", data.verifyInput)

      const userInput = {}
      const verifyInput = data.verifyInput

      Object.keys(verifyInput).forEach(key => {
        let elements = verifyInput[key]
        let id = elements[0]
        let value = elements[1]
        switch (elements[1]) {
          case "id":
            value = id
            break;
          case "value":
            value = store.get("userInfo").userInfo.filter(userInfo => userInfo.id == id).shift().value
            break;
        }
        userInput[key] = snarkjs.bigInt(value)
      })
      store.set("userInput", userInput)
      console.log("Proof Generation, User Input", userInput);
      console.log("Proof Generation, Circuit", data.compiled);
      console.log("Proof Generation, vk_proof", data.vk_proof);
      

      const userInput2 = {}
      Object.keys(userInput).forEach(key=>userInput2[key] = userInput[key].toString())
      jQuery(".input-userInput").val(JSON.stringify(userInput2))
      jQuery(".input-circuit").val("Object, size:"+JSON.stringify(data.compiled).length)
      jQuery(".input-vkProof").val("Object, size:"+JSON.stringify(data.vk_proof).length)

      jQuery(".block-proofGeneration").css("display", "initial")
    },
    error: (XMLHttpRequest) => {
      console.log(XMLHttpRequest)
      alert('Error')
    },
    dataType: 'json'
  })
}

export default () => {
  jQuery(".submit-trustedSetup").click(() => {
    const query = {}
    for (let i = 1; i <= numOfProofGen; i++) {
      let field = jQuery(`.proofGen-field-${i}`).val()
      let value = jQuery(`.proofGen-value-${i}`).val()
      let operand = jQuery(`.proofGen-operand-${i}`).val()
      query[field] = {}
      query[field][operand] = value
    }
    proofGen(query)
  })

  jQuery(".btn-add-proofGen").click(() => {
    numOfProofGen++

    const proofGen = `<div class="form-group form-inline">
    <select class="form-control proofGen-field-${numOfProofGen}">
        <option disabled selected>-- select field --</option>
        <option value="87124">age</option>
    </select>
    &nbsp
    <select class="form-control proofGen-operand-${numOfProofGen}">
      <option selected disabled>-- select --</option>
      <option value="$eq">equals</option>
      <option value="$lt">less than</option>
      <option value="$lte">less than or equals</option value="$lt">
      <option value="$gt">greate than</option value="$lt">
      <option value="$gtg">greate than or equals</option value="$gtg">
    </select>
    &nbsp
    <input type="number" class="form-control proofGen-value-${numOfProofGen}" placeholder="expected value">`

    jQuery(".element-proofGen").append(proofGen)
  })
}
