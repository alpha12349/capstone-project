import jQuery from 'jquery'
import circomlib from 'circomlib'
import store from './store'
import loading from './component-loading'

const fieldValueHash = (field, value, serectKey) => {
  const smtHash = circomlib.poseidon.createHash(6, 8, 57)
  const fieldHash = smtHash([serectKey, field])
  const valueHash = smtHash([serectKey, value])
  return smtHash([valueHash, fieldHash])
}

const stringify = json => {
  let output = {
    userId: json.userId, 
    userInfo: json.userInfo.map(userInfo=>{
      return {id:userInfo.id, label:userInfo.label, hash:userInfo.hash.toString()}
    })
  }
  return JSON.stringify(output)
}

const update = ()=>{
  let value = jQuery(".input-userInfo").val()
  try {
    let input = JSON.parse(value)
    let serectKey = input.serectKey
    let userId = "190283012"
    input.userId = userId
    let output = { userId: userId, userInfo: [] }
    input.userInfo.forEach((userInfo) => {
      let id = userInfo.id
      let val = userInfo.value
      let label = userInfo.label
      output.userInfo.push({
        id: id,
        label: label,
        hash: fieldValueHash(id, val, serectKey).toString()
      })
    })
    jQuery(".input-userSignature").val(stringify(output))
    store.set('userInfo', input)
    store.set('userSignature', output)
  } catch (e) {
    console.log(e)
    jQuery(".input-userSignature").val("Error")
  }
}

export default () => {
  jQuery(".input-userInfo").change(update)
  jQuery(".submit-registration").click(()=>{
    update()
    jQuery(".block-trustedSetup").css("display", "initial")
  })
}
