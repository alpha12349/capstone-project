import snarkjs from "snarkjs";
import store from "./store";
import jQuery from "jquery";

export default () => {
    jQuery(".submit-verification").click(()=>{
        const vk_verifier = store.get('vk_verifier')
        const proof = store.get('proof')
        const publicSignals = store.get('publicSignals')
    
        console.log('test', vk_verifier)
        console.log('test', proof)
        console.log('test',publicSignals)
    
        if(snarkjs.groth.isValid(snarkjs.unstringifyBigInts(vk_verifier), proof, publicSignals)){
            jQuery(".input-result").val("Valid ⭕️")
        } else {
            jQuery(".input-result").val("Invalid ❌")
        }
    })
   
};
