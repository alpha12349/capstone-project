import jQuery from "jquery";

export default {
   on: ()=>{
      jQuery(".loading").css("display", "initial")
   },
   off:()=>{
      jQuery(".loading").css("display", "none")
   }
};
