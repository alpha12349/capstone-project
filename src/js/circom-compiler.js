import multiplier from "../circom/multiplier"
import snarkjs from "snarkjs";

export default () => {
  const circuit = new snarkjs.Circuit(multiplier);

  //Trusted setup.
  const setup = snarkjs.original.setup(circuit);
  const formatedSetup = snarkjs.stringifyBigInts(setup);

  const vk_proof = JSON.stringify(formatedSetup.vk_proof);
  const vk_verifier = JSON.stringify(formatedSetup.vk_verifier);

  //discard the setup
  setup.toxic;

  //Generate proof
  const input = {
    "a": 2,
    "b": 3,
    "c": 6
  };
  const witness = circuit.calculateWitness(input);
  const {proof, publicSignals} = snarkjs.original.genProof(snarkjs.unstringifyBigInts(JSON.parse(vk_proof)), witness);

  let text;
  //Verifier
  if (snarkjs.original.isValid(snarkjs.unstringifyBigInts(JSON.parse(vk_verifier)), proof, publicSignals)) {
    text = "The proof is valid";
  } else {
    text = "The proof is not valid";
  }

  const element = document.createElement("p");
  element.innerHTML = text;
  document.body.appendChild(element)

};

