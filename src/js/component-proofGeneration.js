import jQuery from 'jquery'
import store from './store'
import snarkjs from 'snarkjs'

export default () => {
    jQuery(".submit-proofGeneration").click(() => {
// console.log('click');
        const input = store.get("userInput")
        const userInfo = store.get("userInfo")
        const vk_proof = store.get("vk_proof")
        const compiled = store.get("compiled")

        const circuit = new snarkjs.Circuit(compiled);

        store.set("circuit", circuit)

        input.serectKey = userInfo.serectKey
        let witness;
        try{
           witness = circuit.calculateWitness(input);
        }catch(e){
            alert(e.message)
        }
        
        const { proof, publicSignals } = snarkjs.groth.genProof(snarkjs.unstringifyBigInts(vk_proof), witness)


        const vk_verifier = store.get("vk_verifier")
        store.set("proof", proof)
        store.set("publicSignals", publicSignals)
        console.log("proofGeneration, proof", proof)
        console.log("proofGeneration, publicSignals", publicSignals)
        console.log("proofGeneration, vk_verifier", vk_verifier)
        
        jQuery(".input-proof").val("Object: size: " + JSON.stringify(snarkjs.stringifyBigInts(proof)).length)
        jQuery(".input-publicSignals").val("Object size: " + JSON.stringify(snarkjs.stringifyBigInts(publicSignals)).length)
        jQuery(".input-vkVerifier").val("Object size: " + JSON.stringify(snarkjs.stringifyBigInts(vk_verifier)).length)

      jQuery(".block-verification").css("display", "initial")
      jQuery(".block-result").css("display", "initial")
    })
}
